namespace Amonkeymadethis.Common.Monkeybrain.Attribute.Configure
{
	/**
	 * Metadata attribute to indicate whether to do configuration on the class.
	 */
	public class ConfigureClass : AbstractConfigure
	{
		// Nothing else
	}
}