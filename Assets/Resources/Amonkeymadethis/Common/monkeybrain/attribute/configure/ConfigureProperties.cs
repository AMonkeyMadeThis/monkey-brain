namespace Amonkeymadethis.Common.Monkeybrain.Attribute.Configure
{
	/**
	 * Metadata attribute to indicate whether to do configuration on properties.
	 */
	public class ConfigureProperties: AbstractConfigure
	{
		// Nothing else
	}
}