namespace Amonkeymadethis.Common.Monkeybrain.Attribute.Configure
{
	using System;

	[AttributeUsage(AttributeTargets.Class)]
	/**
	 * Metadata attribure to indicate whether to do configuration of Attributes.
	 */
	public class AbstractConfigure : Attribute
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * Flag to indicate whether to perform reflection on inherited attributes.
		 */
		public bool Inherited { get; set; }
		
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Constructor
		// ----------------------------------
		
		public AbstractConfigure ()
		{
			this.Inherited = false;
		}

		public AbstractConfigure (bool inherited)
		{
			this.Inherited = inherited;
		}
	}
}