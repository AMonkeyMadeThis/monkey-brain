namespace Amonkeymadethis.Common.Monkeybrain.Attribute.Configure
{
	/**
	 * Metadata attribute to indicate whether to do full configuration on the class.
	 */
	public class ConfigureAll : AbstractConfigure
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Constructor
		// ----------------------------------
		
		public ConfigureAll ()
		{
			this.Inherited = false;
		}
		
		public ConfigureAll (bool inherited)
		{
			this.Inherited = inherited;
		}
	}
}