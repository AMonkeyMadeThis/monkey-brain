namespace Amonkeymadethis.Common.Monkeybrain.Attribute.Configure
{
	/**
	 * Metadata attribute to indicate whether to do configuration on methods.
	 */
	public class ConfigureMethods : AbstractConfigure
	{
		// Nothing else
	}
}