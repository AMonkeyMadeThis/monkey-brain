﻿namespace Amonkeymadethis.Common.Monkeybrain.Attribute.Message
{
	using System;

	/**
	 * Message Handler
	 */
	public class MessageHandler : Attribute
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Type that the handler manages
		 */
		public Type Type { get; set; }
		
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Constructor
		// ----------------------------------
		
		public MessageHandler (Type type)
		{
			this.Type = type;
		}
	}
}