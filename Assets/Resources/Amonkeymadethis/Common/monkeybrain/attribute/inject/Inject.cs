﻿namespace Amonkeymadethis.Common.Monkeybrain.Attribute.Inject
{
	using System;
	
	/**
	 * Dependency injection metadata
	 */
	public class Inject : Attribute {}
}