namespace Amonkeymadethis.Common.Monkeybrain.Attribute.Inject
{
	using System;
	
	/**
	 * (Fast) Dependency injection metadata
	 */
	public class FastInject : Attribute
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Configure: Class
		// ----------------------------------

		/**
		 * Type to be injected
		 */
		public Type Type { get; set; }
		/**
		 * Property name
		 */
		public string PropertyName { get; set; }
	}
}