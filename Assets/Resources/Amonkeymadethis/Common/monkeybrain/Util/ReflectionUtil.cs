namespace Amonkeymadethis.Common.Monkeybrain.Util
{
	using System;
	using System.Collections.Generic;
	using System.Reflection;
	using Amonkeymadethis.Common.Monkeybrain.Attribute.Configure;
	using Amonkeymadethis.Common.Monkeybrain.Attribute.Inject;
	using Amonkeymadethis.Common.Monkeybrain.Attribute.Message;
	using Amonkeymadethis.Common.Monkeybrain.Context;
	using Amonkeymadethis.Common.Monkeybrain.Helper;
	
	/**
	 * ReflectionUtil
	 */
	public class ReflectionUtil
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		/**
		 * Returns a list of parameters' types.
		 */
		public static IList<Type> GetParametersTypes (ParameterInfo[] parameters)
		{
			IList<Type> types = new List<Type> ();
			
			foreach (var parameter in parameters)
			{
				types.Add (parameter.ParameterType);
			}
			
			return types;
		}

		/**
		 * 
		 */
		public static void ProcessClassMemberAttributes (IContext context, Type instanceType, ref MonkeyHead instance)
		{
			var attributes = instanceType.GetCustomAttributes (true) as Attribute[];
			
			foreach (var attribute in attributes)
			{
				Type attributeType = attribute.GetType ();
				
				if (attributeType == typeof(ConfigureAll))
				{
					ProcessPropertyAttributes (context, instanceType, ref instance);
					ProcessMethodAttributes (context, instanceType, ref instance);
				}
				else if (attributeType == typeof(ConfigureProperties))
				{
					ProcessPropertyAttributes (context, instanceType, ref instance);
				}
				else if (attributeType == typeof(ConfigureMethods))
				{
					ProcessMethodAttributes (context, instanceType, ref instance);
				}
			}
		}

		/**
		 * 
		 */
		public static void ProcessPropertyAttributes (IContext context, Type instanceType, ref MonkeyHead instance)
		{
			PropertyInfo[] properties = instanceType.GetProperties ();
			
			foreach (PropertyInfo property in properties)
			{
				var attributes = property.GetCustomAttributes (true) as Attribute[];
				
				foreach (Attribute attribute in attributes)
				{
					Type attributeType = attribute.GetType ();
					
					if (attributeType == typeof(Inject))
					{
						InjectionUtil.SetInjection (context, property, ref instance);
					}
					else if (attributeType == typeof(MessageDispatcher))
					{
						InjectionUtil.SetMessageDispatcher (context, property, ref instance);
					}
				}
			}
		}

		/**
		 * 
		 */
		public static void ProcessMethodAttributes (IContext context, Type instanceType, ref MonkeyHead instance)
		{
			MethodInfo[] methods = instanceType.GetMethods();
			
			foreach (MethodInfo method in methods)
			{
				var methodAttributes = method.GetCustomAttributes (true) as Attribute[];
				
				foreach (Attribute attribute in methodAttributes)
				{
					Type attributeType = attribute.GetType ();
					
					if (attributeType == typeof(MessageHandler))
					{
						MessagingUtil.ConfigureMessageHandler (context, method, ref instance, attribute as MessageHandler);
					}
				}
			}
		}
	}
}