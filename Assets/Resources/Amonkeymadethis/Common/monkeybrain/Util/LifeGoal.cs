namespace Amonkeymadethis.Common.Monkeybrain.Util
{
	using System;

	[Flags]
	/**
	 * BitFlag enum of life goals
	 */
	public enum LifeGoals
	{
		Constructed = 1 << 0,
		Awake = 1 << 1,
		OnEnable = 1 << 2,
		Start = 1 << 3,
		FixedUpdate = 1 << 4,
		Update = 1 << 5,
		LateUpdate = 1 << 6,
		OnPreCull = 1 << 7,
		OnBecameVisible = 1 << 8,
		OnBecameInvisible = 1 << 9,
		OnWillRenderObject = 1 << 10,
		OnPreRender = 1 << 11,
		OnRenderObject = 1 << 12,
		OnPostRender = 1 << 13,
		OnRenderImage = 1 << 14,
		OnGUI = 1 << 15,
		OnDrawGizmos = 1 << 16,
		OnDestroy = 1 << 17,
	}
}