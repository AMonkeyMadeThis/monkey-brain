namespace Amonkeymadethis.Common.Monkeybrain.Util
{
	using System;
	using System.Reflection;
	using Amonkeymadethis.Common.Monkeybrain.Context;
	using Amonkeymadethis.Common.Monkeybrain.Context.Dependency;
	using Amonkeymadethis.Common.Monkeybrain.Helper;
	using Amonkeymadethis.Common.Monkeybrain.Message;
	using Amonkeymadethis.Common.Monkeybrain.Util;
	
	/**
	 * InjectionUtil
	 */
	public class InjectionUtil
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		public static void SetInjection (IContext context, PropertyInfo property, ref MonkeyHead instance)
		{
			LogUtil.LogCustomEvent (instance, "Injecting");
			
			IDependency dependency = context.GetDependency (property.PropertyType);
			
			if (dependency != null)
			{
				object value = GetInstance (dependency);
				
				if (value != null)
				{
					object[] parameters = new object[] { value };
					
					InjectProperty (ref instance, property, parameters);
				}
			}
		}
		
		public static object GetInstance (IDependency dependency)
		{
			if (dependency is ISingletonDependency)
			{
				var singleton = dependency as ISingletonDependency;
				return singleton.Instance;
			}
			else if (dependency is IPrototypeDependency)
			{
				var prototype = dependency as IPrototypeDependency;
				return Activator.CreateInstance (prototype.PrototypeType);
			}
			
			return null;
		}
		
		public static void SetMessageDispatcher (IContext context, PropertyInfo property, ref MonkeyHead instance)
		{
			Action<IMessage> receiverAction = context.ReceiveMessage;
			object[] parameters = new object[] { receiverAction };
			
			InjectProperty (ref instance, property, parameters);
		}
		
		public static void InjectProperty (ref MonkeyHead instance, PropertyInfo property, object[] parameters)
		{
			MethodInfo setMethod = property.GetSetMethod ();
			
			setMethod.Invoke (instance, parameters );
		}
	}
}