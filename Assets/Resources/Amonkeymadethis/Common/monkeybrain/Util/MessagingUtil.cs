namespace Amonkeymadethis.Common.Monkeybrain.Util
{
	using UnityEngine;
	using System;
	using System.Collections.Generic;
	using System.Reflection;
	using Amonkeymadethis.Common.Monkeybrain.Attribute.Message;
	using Amonkeymadethis.Common.Monkeybrain.Context;
	using Amonkeymadethis.Common.Monkeybrain.Context.Message;
	using Amonkeymadethis.Common.Monkeybrain.Helper;
	using Amonkeymadethis.Common.Monkeybrain.Message;

	/**
	 * MessagingUtil
	 */
	public class MessagingUtil
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		public static void ConfigureMessageHandler (IContext context, MethodInfo method, ref MonkeyHead instance, MessageHandler attribute)
		{
			Type targetType = attribute.Type;

			var parameters = method.GetParameters ();
			IMessageGateway gateway = GetMessageGateway (context, targetType);
			
			if (gateway != null)
			{
				var action = Delegate.CreateDelegate(typeof(Action<IMessage>), instance, method) as Action<IMessage>;
				gateway.AddAction (action);
				context.AddMessagingGateway (gateway);
			}
		}

		public static IMessageGateway GetMessageGateway (IContext context, Type messageType)
		{
			string targetHash = CryptographyUtil.GetHashString (messageType.Name);

			// Find existing
			foreach (var existingGateway in context.MessageGatewayMap)
			{
				if (existingGateway.ParametersHash == targetHash)
					return existingGateway;
			}
			
			// Build a new one if not found one
			IMessageGateway newGateway = new MessageGateway ();
			newGateway.MessageType = messageType;//ReflectionUtil.GetParametersTypes (parameters);
			newGateway.ParametersHash = targetHash;
			
			return newGateway;
		}

		public static IList<IMessageGateway> GetExistingMessageGateways (IContext context, IMessage message)
		{
			IList<IMessageGateway> gateways = new List<IMessageGateway> ();

			foreach (var gateway in context.MessageGatewayMap)
			{
				if (gateway.MessageType.IsInstanceOfType (message))
					gateways.Add (gateway);
			}
			
			return gateways;
		}
	}
}