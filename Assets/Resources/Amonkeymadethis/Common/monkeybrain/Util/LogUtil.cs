namespace Amonkeymadethis.Common.Monkeybrain.Util
{
	using UnityEngine;
	using System;
	
	/**
	 * LogUtil
	 */
	public class LogUtil
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Lifecycle events
		// ----------------------------------
		
		public static void LogLifeEvent (object instance, LifeGoals stage)
		{
			string pattern = "[{0}] {1}";
			string output = string.Format (pattern, stage, instance.GetType ());
			
			Log (output);
		}
		
		public static void LogLifeEvent (object instance, LifeGoals stage, bool initialised)
		{
			string pattern = "[{0}] {1} (Initialised={2})";
			string output = string.Format (pattern, stage, instance.GetType (), initialised);
			
			Log (output);
		}
		
		// ----------------------------------
		// Custom events
		// ----------------------------------
		
		public static void LogCustomEvent (object instance, string stage)
		{
			string pattern = "[{0}] {1}";
			string output = string.Format (pattern, stage, instance.GetType ());
			
			Log (output);
		}
		
		public static void LogCustomEvent (object instance, string stage, object target)
		{
			string pattern = "[{0}] {1} -> {2}";
			string output = string.Format (pattern, stage, instance.GetType (), target.GetType ());
			
			Log (output);
		}
		
		// ----------------------------------
		// Logging
		// ----------------------------------

		public static void Log (string message)
		{
			Debug.Log (message);

			/*
			if (Application.isWebPlayer)
				Application.ExternalCall("console.log", message);
			*/
		}

	}
}