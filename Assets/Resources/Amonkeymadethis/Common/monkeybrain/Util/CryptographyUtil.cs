namespace Amonkeymadethis.Common.Monkeybrain.Util
{
	using System.Reflection;
	using System.Security.Cryptography;
	using System.Text;
	
	/**
	 * CryptographyUtil
	 */
	public class CryptographyUtil
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		public static string GetParametersHash (ParameterInfo[] parameters)
		{
			string key = string.Empty;
			
			foreach (var parameter in parameters)
			{
				key += parameter.ParameterType.Name;
			}
			
			return GetHashString (key);
		}

		public static byte[] GetMD5Hash (string key)
		{
			MD5 md5 = MD5.Create ();
			byte[] keyBytes = System.Text.Encoding.UTF8.GetBytes (key);
			byte[] hashBytes = md5.ComputeHash ( keyBytes );

			return hashBytes;
		}

		public static string GetHashString (string key)
		{
			byte[] hashBytes = GetMD5Hash (key);
			
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < hashBytes.Length; i++)
			{
				builder.Append(hashBytes[i].ToString("X2"));
			}
			
			return builder.ToString();
		}
	}
}