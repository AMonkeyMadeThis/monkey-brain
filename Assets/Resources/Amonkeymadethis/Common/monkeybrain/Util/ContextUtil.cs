namespace Amonkeymadethis.Common.Monkeybrain.Util
{
	using System;
	using Amonkeymadethis.Common.Monkeybrain.Context;
	using Amonkeymadethis.Common.Monkeybrain.Helper;
	using Amonkeymadethis.Common.Monkeybrain.Util;

	/**
	 * ContextUtil
	 */
	public class ContextUtil
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		// ----------------------------------
		// Registration
		// ----------------------------------
		
		public static void RegisterWithContext (MonkeyHead instance)
		{
			var context = instance.gameObject.GetComponentInParent<IContext> () as IContext;

			if (context != null)
			{
				context.Manage (instance);
				
				ConfigureInstance (context, ref instance);
			}
		}

		// ----------------------------------
		// Configure
		// ----------------------------------
		
		public static void ConfigureInstance (IContext context, MonkeyHead instance)
		{
			LogUtil.LogCustomEvent (context, "ConfigureInstance", instance);
			
			if (instance != null)
			{
				Type instanceType = instance.GetType ();
				
				ReflectionUtil.ProcessClassMemberAttributes (context, instanceType, ref instance);
			}
		}

		public static void ConfigureInstance (IContext context, ref MonkeyHead instance)
		{
			LogUtil.LogCustomEvent (context, "ConfigureInstance", instance);
			
			if (instance != null)
			{
				Type instanceType = instance.GetType ();

				ReflectionUtil.ProcessClassMemberAttributes (context, instanceType, ref instance);
			}
		}
	}
}