/**
 * 
 */
namespace Amonkeymadethis.Common.Monkeybrain.Helper
{
	using System;
	using UnityEngine;
	using Amonkeymadethis.Common.Monkeybrain.Attribute.Configure;
	using Amonkeymadethis.Common.Monkeybrain.Attribute.Inject;
	using Amonkeymadethis.Common.Monkeybrain.Attribute.Message;
	using Amonkeymadethis.Common.Monkeybrain.Context;
	using Amonkeymadethis.Common.Monkeybrain.Command.Request;
	using Amonkeymadethis.Common.Monkeybrain.Message;
	using Amonkeymadethis.Common.Monkeybrain.Util;

	[ConfigureAll(true)]
	/**
	 * MonkeyHead
	 */
	public class MonkeyHead : MonoBehaviour
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		[Inject]
		/**
		 * Context
		 */
		public IContext Context { get; set; }

		[MessageDispatcher]
		/**
		 * Message dispatcher
		 */
		public Delegate Dispatcher { get; set; }

		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Lifecycle
		// ----------------------------------

		/**
		 * Awake
		 */
		public virtual void Awake ()
		{
			LogUtil.LogLifeEvent (this, LifeGoals.Awake);
			// register
			ContextUtil.RegisterWithContext (this);
		}
		
		/**
		 * Start
		 */
		public virtual void Start ()
		{
			LogUtil.LogLifeEvent (this, LifeGoals.Start);
			// register
			ContextUtil.RegisterWithContext (this);
		}

		// ----------------------------------
		// Messaging: Dispatch
		// ----------------------------------

		protected void DispatchMessage (IMessage message)
		{
			if (Dispatcher != null && message != null)
			{
				var dispatcher = Dispatcher as Action<IMessage>;

				if (dispatcher != null)
					dispatcher (message);
			}
		}

		// ----------------------------------
		// Messaging: Receive
		// ----------------------------------

		//[MessageHandler(typeof(IMessage))]
		/**
		 * Handle any IMessage ... IE everything including commands!
		 */
		public virtual void ReceiveMessage (IMessage message)
		{
			Debug.Log ( "ReceiveMessage: " + message.ToString () + " on " + this.ToString ());
		}

		//[MessageHandler(typeof(ICommandTrigger))]
		/**
		 * Handle any ICommandTrigger ... IE all commands!
		 */
		public virtual void ReceiveTrigger (IMessage message)
		{
			Debug.Log ( "ReceiveTrigger: " + message.ToString () + " on " + this.ToString ());
		}
	}
}