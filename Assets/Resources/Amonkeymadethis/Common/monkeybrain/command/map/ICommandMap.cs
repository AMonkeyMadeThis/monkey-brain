namespace Amonkeymadethis.Common.Monkeybrain.Command.Map
{
	using System;
	using Amonkeymadethis.Common.Monkeybrain.Context.Dependency;

	/**
	 * ICommandMap.
	 * 
	 * ICommandMap is just a prototype requirement that is executed as a command rather than injected.
	 */
	public interface ICommandMap : IPrototypeDependency
	{
		// Nothing new
	}
}