﻿namespace Amonkeymadethis.Common.Monkeybrain.Command.Map
{
	using System;
	using Amonkeymadethis.Common.Monkeybrain.Context.Dependency;

	/**
	 * CommandMap.
	 * 
	 * CommandMap is just a prototype requirement that is executed as a command rather than injected.
	 */
	public class CommandMap : PrototypeDependency, ICommandMap
	{
		// Nothing new
	}
}