/**
 * 
 */
namespace Amonkeymadethis.Common.Monkeybrain.Command
{
	using Amonkeymadethis.Common.Monkeybrain.Command.Request;

	/**
	 * ICommand
	 */
	public interface ICommand
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Payload
		 */
		ICommandRequest Payload { get; set; }

		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Lifecycle
		// ----------------------------------

		/**
		 * Awake
		 */
		void Awake ();
		
		/**
		 * Start
		 */
		void Start ();
		
		/**
		 * Execute
		 */
		void Execute ();
		
		// ----------------------------------
		// Outcome
		// ----------------------------------
		
		/**
		 * Result
		 */
		void Result ();
		
		/**
		 * Failure
		 */
		void Failure ();
	}
}