﻿/**
 * 
 */
namespace Amonkeymadethis.Common.Monkeybrain.Command
{
	using Amonkeymadethis.Common.Monkeybrain.Helper;
	using Amonkeymadethis.Common.Monkeybrain.Command.Request;
	using Amonkeymadethis.Common.Monkeybrain.Command.Result;
	using Amonkeymadethis.Common.Monkeybrain.Util;

	/**
	 * AbstractCommand
	 */
	public class AbstractCommand : MonkeyHead, ICommand
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * Payload
		 */
		public ICommandRequest Payload { get; set; }
		
		// ----------------------------------
		// Helper Flags
		// ----------------------------------

		/**
		 * HasPayload
		 */
		protected bool HasPayload() { return (Payload != null); }
		/**
		 * HasResponder
		 */
		protected bool HasResponder() { return (HasPayload () && Payload.Responder != null); }
		/**
		 * HasResultHandler
		 */
		protected bool HasResultHandler() { return (HasResponder () && Payload.Responder.Result != null); }
		/**
		 * HasFaultHandler
		 */
		protected bool HasFaultHandler() { return (HasResponder () && Payload.Responder.Fault != null); }

		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Lifecycle
		// ----------------------------------

		/**
		 * Execute
		 */
		public virtual void Execute ()
		{
			LogUtil.LogCustomEvent (this, "Execute");

			Result ();
		}
		
		/**
		 * End
		 */
		public virtual void End ()
		{
			LogUtil.LogCustomEvent (this, "End");

			Destroy (this);
		}

		// ----------------------------------
		// Outcome
		// ----------------------------------
		
		/**
		 * Result
		 */
		public virtual void Result ()
		{
			LogUtil.LogCustomEvent (this, "Result");
			
			ICommandResponse result = new AbstractCommandResponse (true);
			InvokeResponderResult (result);
			// Destroy
			End ();
		}

		/**
		 * Result
		 */
		public virtual void Result (object data)
		{
			LogUtil.LogCustomEvent (this, "Result");

			ICommandResponse result = new AbstractCommandResponse (data);
			InvokeResponderResult (result);
			// Destroy
			End ();
		}
		
		/**
		 * Failure
		 */
		public virtual void Failure ()
		{
			LogUtil.LogCustomEvent (this, "Failure");
			
			ICommandResponse result = new AbstractCommandResponse (false);
			InvokeResponderFault (result);
			// Destroy
			End ();
		}
		
		// ----------------------------------
		// Invoke Responder
		// ----------------------------------

		protected void InvokeResponderResult (ICommandResponse result)
		{
			if (HasResultHandler ())
				Payload.Responder.Result.Invoke (result);
		}

		protected void InvokeResponderFault (ICommandResponse result)
		{
			if (HasFaultHandler ())
				Payload.Responder.Fault.Invoke (result);
		}
	}
}