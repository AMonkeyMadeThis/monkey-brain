/**
 * 
 */
namespace Amonkeymadethis.Common.Monkeybrain.Command.Request
{
	using Amonkeymadethis.Common.Monkeybrain.Message;
	using Amonkeymadethis.Common.Monkeybrain.Responder;

	/**
	 * ICommandRequest
	 */
	public interface ICommandRequest : IMessage
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Command Lifecycle
		// ----------------------------------

		/**
		 * Responder
		 */
		IResponder Responder { get; set; }
	}
}