/**
 * 
 */
namespace Amonkeymadethis.Common.Monkeybrain.Command.Request
{
	using Amonkeymadethis.Common.Monkeybrain.Responder;
	
	/**
	 * AbstractCommandRequest
	 */
	public class AbstractCommandRequest : ICommandRequest
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Lifecycle
		// ----------------------------------
		
		/**
         * Responder
         */
		public IResponder Responder { get; set; }
	}
}