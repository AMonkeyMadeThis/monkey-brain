/**
 * 
 */
namespace Amonkeymadethis.Common.Monkeybrain.Command.Result
{
	/**
	 * AbstractCommandResponse
	 */
	public class AbstractCommandResponse : ICommandResponse
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Success
		// ----------------------------------

		private bool _success;

		/**
		 * Success
		 */
		public bool Success { get; set; }
		
		// ----------------------------------
		// Data
		// ----------------------------------
		
		private object _data;

		/**
		 * Data
		 */
		public object Data
		{
			get { return _data; }
		}
		
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Constructor
		// ----------------------------------

		/**
		 * Lazy constructor, must have gone well ;p
		 */
		public AbstractCommandResponse ()
		{
			// assume true?!
			_success = true;
		}

		/**
		 * Volatile constructor, could go either way ...
		 */
		public AbstractCommandResponse (bool success)
		{
			// it was a ...
			_success = success;
		}

		/**
		 * Positive constructor
		 */
		public AbstractCommandResponse (object data)
		{
			// set data
			_data = data;
			// we got data, so ...
			_success = true;
		}
	}
}