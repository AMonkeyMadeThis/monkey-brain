/**
 * 
 */
namespace Amonkeymadethis.Common.Monkeybrain.Command.Result
{
	/**
	 * ICommandResponse
	 */
	public interface ICommandResponse
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Success
		 */
		bool Success { get; }
		/**
		 * Data
		 */
		object Data { get; }
	}
}