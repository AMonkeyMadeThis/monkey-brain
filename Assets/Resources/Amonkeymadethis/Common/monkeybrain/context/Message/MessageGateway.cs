namespace Amonkeymadethis.Common.Monkeybrain.Context.Message
{
	using System;
	using System.Collections.Generic;
	using Amonkeymadethis.Common.Monkeybrain.Message;
	
	/**
	 * MessageGateway
	 */
	public class MessageGateway : IMessageGateway
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * MessageType
		 */
		public Type MessageType { get; set; }
		/**
		 * ParametersHash
		 */
		public string ParametersHash { get; set; }
		/**
		 * Actions
		 */
		public IList<Action<IMessage>> Actions { get; set; }

		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Constructor
		// ----------------------------------

		public MessageGateway ()
		{
			Actions = new List<Action<IMessage>> ();
		}
		
		// ----------------------------------
		// Actions
		// ----------------------------------

		public void AddAction (Action<IMessage> action)
		{
			if (Actions == null)
				Actions = new List<Action<IMessage>> ();

			Actions.Add (action);
		}
	}
}