namespace Amonkeymadethis.Common.Monkeybrain.Context.Message
{
	using System;
	using System.Collections.Generic;
	using Amonkeymadethis.Common.Monkeybrain.Message;

	/**
	 * IMessageGateway
	 */
	public interface IMessageGateway
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * MessageType
		 */
		Type MessageType { get; set; }
		/**
		 * ParametersHash
		 */
		string ParametersHash { get; set; }
		/**
		 * Actions
		 */
		IList<Action<IMessage>> Actions { get; set; }
		
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		// ----------------------------------
		// Actions
		// ----------------------------------
		
		void AddAction (Action<IMessage> action);
	}
}