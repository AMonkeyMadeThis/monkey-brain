﻿/**
 * 
 */
namespace Amonkeymadethis.Common.Monkeybrain.Context
{
	using System;
	using System.Collections;
    using System.Collections.Generic;
	using UnityEngine;
	using Amonkeymadethis.Common.Monkeybrain.Command.Map;
	using Amonkeymadethis.Common.Monkeybrain.Context.Controller;
	using Amonkeymadethis.Common.Monkeybrain.Context.Dependency;
	using Amonkeymadethis.Common.Monkeybrain.Context.Message;
	using Amonkeymadethis.Common.Monkeybrain.Helper;
	using Amonkeymadethis.Common.Monkeybrain.Message;
	using Amonkeymadethis.Common.Monkeybrain.Util;

	[RequireComponent(typeof(CommandController))]
	/**
	 * AbstractContext
	 */
	public class AbstractContext : MonoBehaviour, IContext
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		protected bool _isInitialised;

		/**
		 * Is Initialised
		 */
		protected bool IsInitialised
		{
			get
			{
				if (!_isInitialised)
					_isInitialised = Initialise ();

				return _isInitialised;
			}
		}

		// ----------------------------------
		// Taxonomy
		// ----------------------------------

		/**
		 * Children
		 */
		protected IList<IContext> children;
		/**
		 * Managed
		 */
		protected IList<MonkeyHead> managed;
		
		// ----------------------------------
		// Components
		// ----------------------------------

		/**
		 * CommandController
		 */
		public CommandController CommandController { get; set; }

		// ----------------------------------
		// Dependency Injection
		// ----------------------------------

		/**
         * Singleton Dependency Pool
         */
		public IList<ISingletonDependency> SingletonDependencyPool { get; set; }
		/**
         * Prototype Dependency Map
         */
		public IList<IPrototypeDependency> PrototypeDependencyMap { get; set; }
		
		// ----------------------------------
		// Messaging
		// ----------------------------------

		/**
         * Message Gateway Map
         */
		public IList<IMessageGateway> MessageGatewayMap { get; set; }

		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Constructor
		// ----------------------------------

		public AbstractContext ()
		{
			// Note testing IsInitialised will force initialisation.
			LogUtil.LogLifeEvent (this, LifeGoals.Constructed, IsInitialised);
		}

		// ----------------------------------
		// Lifecycle
		// ----------------------------------

		/**
		 * Awake
		 */
		public virtual void Awake ()
		{
			LogUtil.LogLifeEvent (this, LifeGoals.Awake, IsInitialised);
		}

		/**
		 * Start
		 */
		public virtual void Start ()
		{
			LogUtil.LogLifeEvent (this, LifeGoals.Start, IsInitialised);
		}
		
		// ----------------------------------
		// Init
		// ----------------------------------

		protected bool Initialise ()
		{
			LogUtil.LogCustomEvent (this, "Initialise");

			if (!_isInitialised)
			{
				//RegisterWithParentContext ();
				InitMaps ();
				AddThisToSingletonPool ();
				MapTypes ();
				ConfigureManaged ();
			}
			
			return true;
		}
		
		private void InitMaps ()
		{
			managed = new List<MonkeyHead> ();
			SingletonDependencyPool = new List<ISingletonDependency> ();
			PrototypeDependencyMap = new List<IPrototypeDependency> ();
			MessageGatewayMap = new List<IMessageGateway>  ();
		}

		/**
		 * Things need context so add it as a dependency. This is a special case 
		 * so need to add (this) context to pool directly.
		 */
		private void AddThisToSingletonPool ()
		{
			ISingletonDependency context = new SingletonDependency ();
			context.Requirement = typeof(IContext);
			context.Instance = this;
			SingletonDependencyPool.Add (context);
		}

		// ----------------------------------
		// Taxonomy
		// ----------------------------------
		
		protected void RegisterWithParentContext ()
		{
			if (transform != null && transform.parent != null)
			{
				IContext parent = transform.parent.GetComponentInParent<IContext> () as IContext;
				bool isThis = parent == this;
				
				if (parent != null)
					parent.AddChildContext (this);
			}
		}

		public void AddChildContext (IContext child)
		{
			LogUtil.LogCustomEvent (this, "AddChildContext", child);

			if (children == null)
				children = new List<IContext> ();

			children.Add (child);
		}

		// ----------------------------------
		// Dependency Management
		// ----------------------------------

		private void ConfigureManaged ()
		{
			foreach (var instance in managed)
			{
				ContextUtil.ConfigureInstance (this, instance);
			}
		}

		public void Manage (MonkeyHead instance)
		{
			LogUtil.LogCustomEvent (this, "Manage", instance);

			if (IsInitialised)
			{
				if (managed == null)
					managed = new List<MonkeyHead> ();

				managed.Add ( instance );
			}
		}
		
		/**
		 * Get Dependency
		 */
		public IDependency GetDependency (Type requiredType)
		{
			// Check singletons
			foreach(var singleton in SingletonDependencyPool)
			{
				if (singleton.Requirement == requiredType)
					return singleton;
			}

			// Check protoypes
			foreach(var prototype in PrototypeDependencyMap)
			{
				if (prototype.Requirement == requiredType)
					return prototype;
			}

			return null;
		}

		// ----------------------------------
		// Mapping
		// ----------------------------------
		
		/**
		 * Map Types
		 */
		public virtual void MapTypes ()
		{
			throw new System.NotImplementedException ();
		}

		/**
		 * Map Singleton
		 */
		protected void MapSingleton (Type requirement, Type prototype)
		{
			if (SingletonDependencyPool == null)
				SingletonDependencyPool = new List<ISingletonDependency> ();
			
			ISingletonDependency dependency = new SingletonDependency ();
			dependency.Requirement = requirement;
			dependency.Instance = Activator.CreateInstance (prototype);
			
			SingletonDependencyPool.Add (dependency);
		}

		/**
		 * Map Prototype
		 */
		protected void MapPrototype (Type requirement, Type prototype)
		{
			if (PrototypeDependencyMap == null)
				PrototypeDependencyMap = new List<IPrototypeDependency> ();

			IPrototypeDependency dependency = new PrototypeDependency ();
			dependency.Requirement = requirement;
			dependency.PrototypeType = prototype;
			
			PrototypeDependencyMap.Add (dependency);
		}

		/**
		 * Map Command
		 */
		protected void MapCommand (Type triggerType, Type commandType)
		{
			if (PrototypeDependencyMap == null)
				PrototypeDependencyMap = new List<IPrototypeDependency> ();

			ICommandMap map = new CommandMap ();
			map.Requirement = triggerType;
			map.PrototypeType = commandType;

			PrototypeDependencyMap.Add (map);
		}
		
		// ----------------------------------
		// Messaging
		// ----------------------------------

		/**
		 * Add Messaging Gateway
		 */
		public void AddMessagingGateway (IMessageGateway gateway)
		{
			if (MessageGatewayMap == null)
				MessageGatewayMap = new List<IMessageGateway> ();

			MessageGatewayMap.Add (gateway);
		}

		public void ReceiveMessage (IMessage message)
		{
			LogUtil.LogCustomEvent (this, "ReceiveMessage", message);

			IList<IMessageGateway> gateways = MessagingUtil.GetExistingMessageGateways (this, message);

			if (gateways != null && gateways.Count > 0)
			{
				// loop and invoke actions ...
				foreach (var gateway in gateways)
				{
					foreach (var action in gateway.Actions)
					{
						action.Invoke (message);
					}
				}
			}
		}
	}
}