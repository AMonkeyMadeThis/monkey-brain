/**
 * 
 */
namespace Amonkeymadethis.Common.Monkeybrain.Context
{
	using System;
	using System.Collections.Generic;
	using Amonkeymadethis.Common.Monkeybrain.Context.Dependency;
	using Amonkeymadethis.Common.Monkeybrain.Context.Message;
	using Amonkeymadethis.Common.Monkeybrain.Command.Map;
	using Amonkeymadethis.Common.Monkeybrain.Helper;
	using Amonkeymadethis.Common.Monkeybrain.Message;

	/**
	 * ICommandMediator
	 */
	public interface IContext
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Dependency Injection
		// ----------------------------------

		/**
         * Singleton Dependency Pool
         */
		IList<ISingletonDependency> SingletonDependencyPool { get; set; }
		/**
         * Prototype Dependency Map
         */
		IList<IPrototypeDependency> PrototypeDependencyMap { get; set; }
		
		// ----------------------------------
		// Messaging
		// ----------------------------------

		/**
         * Message Gateway Map
         */
		IList<IMessageGateway> MessageGatewayMap { get; set; }

		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Taxonomy
		// ----------------------------------
		
		void AddChildContext (IContext child);

		// ----------------------------------
		// Messaging
		// ----------------------------------
		
		/**
		 * Add Messaging Gateway
		 */
		void AddMessagingGateway (IMessageGateway gateway);
		/**
		 * Receive Message
		 */
		void ReceiveMessage (IMessage message);
		
		// ----------------------------------
		// Dependency Management
		// ----------------------------------

		/**
		 * Manage
		 */
		void Manage (MonkeyHead monkeyHead);

		/**
		 * Get Dependency
		 */
		IDependency GetDependency (Type propertyType);
	}
}