/**
 * 
 */
namespace Amonkeymadethis.Common.Monkeybrain.Context.Controller
{
	using System;
	using Amonkeymadethis.Common.Monkeybrain.Attribute.Message;
	using Amonkeymadethis.Common.Monkeybrain.Command;
	using Amonkeymadethis.Common.Monkeybrain.Command.Map;
	using Amonkeymadethis.Common.Monkeybrain.Command.Request;
	using Amonkeymadethis.Common.Monkeybrain.Context.Dependency;
	using Amonkeymadethis.Common.Monkeybrain.Helper;
	using Amonkeymadethis.Common.Monkeybrain.Message;
	using Amonkeymadethis.Common.Monkeybrain.Util;

	/**
	 * CommandController
	 */
	public class CommandController : MonkeyHead
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		// ----------------------------------
		// Command Execution
		// ----------------------------------
		
		[MessageHandler(typeof(ICommandRequest))]
		/**
		 * Trigger Command.
		 * 
		 * Searches through each of the instances of <code>ICommandMap</code> in <code>maps/<code>
		 * when it finds a matching trigger type it will spawn that type of command and pass the trigger message.
		 * 
		 * @argument instance of a class that implements <code>ICommandTrigger</code>
		 */
		public virtual void TriggerCommand (IMessage message)
		{
			LogUtil.LogCustomEvent (this, "TriggerCommand", message);

			var trigger = message as ICommandRequest;

			if (trigger != null)
			{
				Type instanceType = trigger.GetType ();
				
				foreach (var item in Context.PrototypeDependencyMap)
				{
					var commandMap = item as ICommandMap;
					
					if (commandMap != null)
					{
						Type mapType = commandMap.Requirement;
						
						if (mapType == instanceType)
							SpawnCommand (commandMap.PrototypeType, trigger);
					}
				}
			}
		}
		
		/**
		 * SpawnCommand.
		 * 
		 * Adds a 'command' component to this game object. It will 'Execute' when it goes through the normal component lifecycle, then remove itself when done.
		 */
		protected void SpawnCommand (Type commandType, ICommandRequest trigger)
		{
			var command = gameObject.AddComponent (commandType) as ICommand;
			command.Payload = trigger;
			command.Execute ();
		}
	}
}