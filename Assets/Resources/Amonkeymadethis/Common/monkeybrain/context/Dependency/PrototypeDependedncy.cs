namespace Amonkeymadethis.Common.Monkeybrain.Context.Dependency
{
	using System;
	
	/**
	 * PrototypeDependency
	 */
	public class PrototypeDependency : AbstractDependency, IPrototypeDependency
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * PrototypeType
		 */
		public Type PrototypeType { get; set; }
	}
}