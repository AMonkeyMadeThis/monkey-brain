namespace Amonkeymadethis.Common.Monkeybrain.Context.Dependency
{
	using System;
	
	/**
	 * IPrototypeDependency
	 */
	public interface IPrototypeDependency : IDependency
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		/**
		 * PrototypeType
		 */
		Type PrototypeType { get; set; }
	}
}