namespace Amonkeymadethis.Common.Monkeybrain.Context.Dependency
{
	using System;
	
	/**
	 * IDependency
	 */
	public interface IDependency
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Requirement
		 */
		Type Requirement { get; set; }
	}
}