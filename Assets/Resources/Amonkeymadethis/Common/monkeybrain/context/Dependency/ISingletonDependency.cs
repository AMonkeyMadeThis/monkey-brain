namespace Amonkeymadethis.Common.Monkeybrain.Context.Dependency
{
	using System;
	
	/**
	 * ISingletonDependency
	 */
	public interface ISingletonDependency : IDependency
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Instance
		 */
		object Instance { get; set; }
	}
}