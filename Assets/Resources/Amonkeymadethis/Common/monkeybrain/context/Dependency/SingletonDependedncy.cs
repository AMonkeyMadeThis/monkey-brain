namespace Amonkeymadethis.Common.Monkeybrain.Context.Dependency
{
	using System;
	
	/**
	 * SingletonDependency
	 */
	public class SingletonDependency : AbstractDependency, ISingletonDependency
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * Instance
		 */
		public object Instance { get; set; }
	}
}