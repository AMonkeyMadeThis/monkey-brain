namespace Amonkeymadethis.Common.Monkeybrain.Context.Dependency
{
	using System;
	
	/**
	 * AbstractDependency
	 */
	public class AbstractDependency : IDependency
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * Requirement
		 */
		public Type Requirement { get; set; }
	}
}