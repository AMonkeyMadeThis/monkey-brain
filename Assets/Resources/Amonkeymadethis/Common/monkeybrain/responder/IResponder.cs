/**
 * 
 */
namespace Amonkeymadethis.Common.Monkeybrain.Responder
{
	using System;
	using Amonkeymadethis.Common.Monkeybrain.Command.Result;

	/**
	 * IResponder
	 */
	public interface IResponder
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		// ----------------------------------
		// Lifecycle
		// ----------------------------------

		/**
		 * Result
		 */
		Action<ICommandResponse> Result { get; set; }
		/**
		 * Fault
		 */
		Action<ICommandResponse> Fault { get; set; }
	}
}