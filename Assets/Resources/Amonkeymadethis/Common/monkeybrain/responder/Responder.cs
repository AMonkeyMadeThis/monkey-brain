/**
 * 
 */
namespace Amonkeymadethis.Common.Monkeybrain.Responder
{
	using System;
	using Amonkeymadethis.Common.Monkeybrain.Command.Result;

	/**
	 * IResponder
	 */
	public class Responder : IResponder
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * Result
		 */
		public virtual Action<ICommandResponse> Result { get; set; }
		/**
		 * Fault
		 */
		public virtual Action<ICommandResponse> Fault { get; set; }

		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		public Responder (Action<ICommandResponse> result, Action<ICommandResponse> fault)
		{
			this.Result = result;
			this.Fault = fault;
		}
	}
}