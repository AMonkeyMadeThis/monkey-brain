﻿namespace com.amonkeymadethis.common.audio.music.bgm.model
{
	using UnityEngine;
	using System.Collections;
	using System.Xml.Serialization;

	[XmlRoot(ElementName="bgm")]
	/**
	 * BGM model
	 */
	public class BGM
	{
		[XmlAttribute(AttributeName="path")]
		/**
		 * Base url
		 */
		public string path { get; set; }

		[XmlElement(ElementName="track")]
		/**
		 * Tracks
		 */
		public Track[] tracks { get; set; }
	}
}