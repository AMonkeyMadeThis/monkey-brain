﻿namespace com.amonkeymadethis.common.audio.music.bgm.model
{
	using UnityEngine;
	using System.Collections;
	using System.Xml.Serialization;
	
	[XmlRoot(ElementName="bgm")]
	/**
	 * Track
	 */
	public class Track
	{
		[XmlAttribute(AttributeName="artist")]
		/**
		 * Artist name
		 */
		public string artist { get; set; }

		[XmlAttribute(AttributeName="name")]
		/**
		 * Track name
		 */
		public string name { get; set; }
		
		[XmlAttribute(AttributeName="file")]
		/**
		 * File path
		 */
		public string file { get; set; }
	}
}