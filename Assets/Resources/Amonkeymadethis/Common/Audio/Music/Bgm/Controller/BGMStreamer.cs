namespace com.amonkeymadethis.common.audio.music.bgm.controller
{
	using UnityEngine;
	using System.Collections;
	using System.Xml.Serialization;
	using System.IO;
	using com.amonkeymadethis.common.audio.music.bgm.model;

	[RequireComponent(typeof(AudioSource))]
	/**
	 * 
	 */
	public class BGMStreamer : MonoBehaviour
	{
		public string baseUrl;

		public string config;

		public Track current;

        protected bool hasLoaded = false;
        protected bool skipRequested = true;
		protected AudioSource audioSource;
		protected AudioClip clipA;
		protected AudioClip clipB;
		protected BGM bgm;

		/**
		 * Start
		 */
		void Start ()
		{
			CollectComponents ();
			LoadConfig ();
		}

		void LateUpdate ()
		{
            CheckSkip ();
            CheckPlayNext ();
		}

        protected void CheckSkip ()
        {
            if (!skipRequested)
            {
                skipRequested = Input.GetKeyUp("s");
            }
        }

        protected void CheckPlayNext ()
        {
            if (hasLoaded && (!audioSource.isPlaying || skipRequested))
            {
                PlayNextTrack();
            }
        }

		protected void CollectComponents ()
		{
			audioSource = GetComponent<AudioSource> ();
		}

		protected void LoadConfig ()
		{
			StartCoroutine ( LoadConfigThread (baseUrl + config) );
		}

		IEnumerator LoadConfigThread (string url)
		{
			Debug.Log ( "Loading: " + url );

			WWW www = new WWW(url);

			yield return www;
			
			Debug.Log ( "Loaded: " + url );

			XmlSerializer serialiser = new XmlSerializer (typeof(BGM));
			Stream reader = new MemoryStream ( www.bytes );
			bgm = serialiser.Deserialize ( reader ) as BGM;
			reader.Close ();

			PreloadNextTrack ();
		}

		protected void PlayNextTrack ()
		{
            // clear skip request
            skipRequested = false;
			SwapInputs ();
			PreloadNextTrack ();
		}
		
		protected void SwapInputs ()
		{
			clipA = clipB;
			
			audioSource.clip = clipA;
			audioSource.loop = false;
			audioSource.Play ();
		}

		protected void PreloadNextTrack ()
		{
			StartCoroutine ( PreloadNextTrackThread () );
		}

		IEnumerator PreloadNextTrackThread ()
		{
			hasLoaded = false;

			Track track = GetRandomTrack ();
			string file = baseUrl + bgm.path + track.file;
			WWW www = new WWW (file);

			Debug.Log ("Preloading: "+file);

			yield return www;
			
			clipB = www.GetAudioClip (true, true);
			hasLoaded = true;

            Debug.Log("Preloaded: " + file);
		}

		protected Track GetRandomTrack ()
		{
			Track[] tracks = bgm.tracks;
			Track next = current;

			while (next == current)
			{
				int i = Random.Range (0, tracks.Length-1);
				next = tracks[i];
			}

			return next;
		}
	}
}