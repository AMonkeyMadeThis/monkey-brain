/**
 * 
 */
namespace Amonkeymadethis.Common.Facebook.Model.Vo
{
	using System.Collections.Generic;

	/**
	 * FacebookFriendsList
	 */
	public class FacebookFriendsList : IFacebookFriendsList
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * Friends
		 */
		public IList<IFacebookFriendProfile> data { get; set; }
	}
}