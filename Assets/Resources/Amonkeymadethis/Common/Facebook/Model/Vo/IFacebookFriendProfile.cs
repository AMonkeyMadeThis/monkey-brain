/**
 * 
 */
namespace Amonkeymadethis.Common.Facebook.Model.Vo
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;
	
	/**
	 * IFacebookFriendProfile
	 */
	public interface IFacebookFriendProfile : IFacebookProfile
	{
		// Nothing new
	}
}