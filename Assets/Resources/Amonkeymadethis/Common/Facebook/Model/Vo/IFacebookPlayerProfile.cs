/**
 * 
 */
namespace Amonkeymadethis.Common.Facebook.Model.Vo
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;
	
	/**
	 * IFacebookPlayerProfile
	 */
	public interface IFacebookPlayerProfile : IFacebookProfile
	{
		// Nothing new
	}
}