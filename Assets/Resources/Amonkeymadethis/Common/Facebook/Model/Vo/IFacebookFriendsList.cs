/**
 * 
 */
namespace Amonkeymadethis.Common.Facebook.Model.Vo
{
	using System.Collections.Generic;
	
	/**
	 * FacebookFriendsList
	 */
	public interface IFacebookFriendsList
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Friends
		 */
		IList<IFacebookFriendProfile> data { get; set; }
	}
}