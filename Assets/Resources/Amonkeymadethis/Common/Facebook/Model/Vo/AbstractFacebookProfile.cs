﻿/**
 * 
 */
namespace Amonkeymadethis.Common.Facebook.Model.Vo
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;
	
	/**
	 * AbstractFacebookProfile
	 */
	public class AbstractFacebookProfile : IFacebookProfile
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Personal Details
		// ----------------------------------
		
		/**
		 * Id
		 */
		public string Id { get; set; }
		/**
		 * FirstName
		 */
		public string FirstName { get; set; }
		/**
		 * LastName
		 */
		public string LastName { get; set; }
		
		// ----------------------------------
		// Media
		// ----------------------------------
		
		/**
		 * Picture
		 */
		public object Picture { get; set; }
	}
}