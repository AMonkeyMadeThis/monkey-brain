﻿/**
 * 
 */
namespace Amonkeymadethis.Common.Facebook.Model.Vo
{
	/**
	 * FacebookFriendProfile
	 */
	public class FacebookFriendProfile : AbstractFacebookProfile, IFacebookFriendProfile
	{
		// Nothing new
	}
}