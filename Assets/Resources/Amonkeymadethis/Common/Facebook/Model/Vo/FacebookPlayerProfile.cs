﻿/**
 * 
 */
namespace Amonkeymadethis.Common.Facebook.Model.Vo
{
	/**
	 * FacebookPlayerProfile
	 */
	public class FacebookPlayerProfile : AbstractFacebookProfile, IFacebookPlayerProfile
	{
		// Nothing new
	}
}