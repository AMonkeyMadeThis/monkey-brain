/**
 * 
 */
namespace Amonkeymadethis.Common.Facebook.Model.Vo
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;
	
	/**
	 * IFacebookProfile
	 */
	public interface IFacebookProfile
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Personal Details
		// ----------------------------------

		/**
		 * Id
		 */
		string Id { get; set; }
		/**
		 * FirstName
		 */
		string FirstName { get; set; }
		/**
		 * LastName
		 */
		string LastName { get; set; }
		
		// ----------------------------------
		// Media
		// ----------------------------------

		/**
		 * Picture
		 */
		object Picture { get; set; }
		
		// ----------------------------------
		// Lifecycle
		// ----------------------------------
		
		//IList<Friend> friends { get; set; }
		//IList<Friend> invitable_friends { get; set; }
	}
}