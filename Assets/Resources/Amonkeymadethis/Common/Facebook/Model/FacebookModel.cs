namespace Amonkeymadethis.Common.Facebook.Model
{
	using System.Collections.Generic;
	using Amonkeymadethis.Common.Facebook.Model.Vo;

	/**
	 * FacebookModel
	 */
	public class FacebookModel
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * Player
		 */
		public IFacebookPlayerProfile Player;
		/**
		 * Friends
		 */
		public IFacebookFriendsList Friends { get; set; }
		/**
		 * InvitableFriends
		 */
		public IFacebookFriendsList InvitableFriends { get; set; }
	}
}