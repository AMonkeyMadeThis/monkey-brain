﻿namespace Amonkeymadethis.Common.Facebook.UI.View.LogIn
{
	using Amonkeymadethis.Common.Monkeybrain.Command.Result;
	using Amonkeymadethis.Common.Monkeybrain.Helper;
	using Amonkeymadethis.Common.Monkeybrain.Responder;
	using Amonkeymadethis.Common.Monkeybrain.Util;
	using Amonkeymadethis.Common.Facebook.Command.Authentication;
	
	public class LogInMediator : MonkeyHead
	{
		// ----------------------------------
		// Log in
		// ----------------------------------
		
		protected void LogIn ()
		{
			LogUtil.LogCustomEvent (this, "LogIn");

			FacebookLogInRequest message = new FacebookLogInRequest ();
			message.Permissions = Scope.Basic;
			message.Responder = new Responder (LogInResult, LogInFault);
			
			DispatchMessage (message);
		}
		
		protected void LogInResult (ICommandResponse result)
		{
			LogUtil.LogCustomEvent (this, "LogInResult", result);
			
			//LoadProfile ();
		}
		
		protected void LogInFault (ICommandResponse result)
		{
			LogUtil.LogCustomEvent (this, "LogInFault", result);
		}
	}
}