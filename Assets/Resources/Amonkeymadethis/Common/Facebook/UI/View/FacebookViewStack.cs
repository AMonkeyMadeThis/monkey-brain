﻿namespace Amonkeymadethis.Common.Facebook.UI.View
{
	using Amonkeymadethis.Common.UI.Components;
	
	/**
	 * FacebookViewStack
	 */
	public class FacebookViewStack : ViewStack
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Lifecycle
		// ----------------------------------

		/**
		 * SelectedContent as described in FacebookContent as an easier way of dealing with SelectedIndex
		 */
		public FacebookContent SelectedContent;

		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		// ----------------------------------
		// Lifecycle
		// ----------------------------------

		public override void LateUpdate ()
		{
			// Bit of a hack as if the SelectedContent property has a getter/setter
			// then it doesnt show in the editor. This one could be temporary though.
			//
			SelectedIndex = (int)SelectedContent;
		}
	}
}