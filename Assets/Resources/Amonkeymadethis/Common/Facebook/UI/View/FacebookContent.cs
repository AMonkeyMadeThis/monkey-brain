namespace Amonkeymadethis.Common.Facebook.UI.View
{
	using System;

	[Flags]
	/**
	 * FacebookContent
	 */
	public enum FacebookContent
	{
		Hidden = 1 << 0,
		Initialising = 1 << 1,
		UnAuthenticated = 1 << 2,
		Authenticating = 1 << 3,
		Authenticated = 1 << 4,
	}
}