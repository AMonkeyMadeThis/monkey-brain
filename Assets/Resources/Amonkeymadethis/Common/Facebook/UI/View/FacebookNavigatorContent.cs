﻿namespace Amonkeymadethis.Common.Facebook.UI.View
{
	using Amonkeymadethis.Common.UI.Components;

	/**
	 * FacebookNavigatorContent
	 */
	public class FacebookNavigatorContent : NavigatorContent
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * Content as described in FacebookContent as an easier way of dealing with ContentIndex
		 */
		public FacebookContent Content;
		
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		public override void Start ()
		{
			base.Start ();

			// Hack to allow the editor to show content as wiring this directly
			// in getters/setters will cause it not to show in editor and so be
			// completely uselss.
			//
			ContentIndex = (int)Content;
		}
	}
}