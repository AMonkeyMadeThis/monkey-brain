namespace Amonkeymadethis.Common.Facebook.Util
{
	using System.Collections;
	using System.Collections.Generic;
	using Amonkeymadethis.Common.Facebook.Model;
	using Amonkeymadethis.Common.Facebook.Model.Vo;
	
	/**
	 * FacebookUtil
	 */
	public class FacebookUtil
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		public static void DeserialiseData (string source, FacebookModel model)
		{
			var data = FB.DeserializeJson (source) as Dictionary<string, object>;
			
			// Player
			model.Player = new FacebookPlayerProfile ();
			PopulateProfile (data, model.Player as IFacebookProfile);
			
			// Friends
			model.Friends = DeserializeFriends (data, "friends");
			
			// Invitable Friends
			model.InvitableFriends = DeserializeFriends (data, "invitable_friends");
		}
		
		public static IFacebookFriendsList DeserializeFriends (Dictionary<string, object> dictionary, string type)
		{
			IFacebookFriendsList result = new FacebookFriendsList ();
			result.data = new List<IFacebookFriendProfile> ();
			
			var friends = GetDictionaryValue (dictionary, type) as Dictionary<string, object>;
			
			if (friends != null)
			{
				var data = GetDictionaryValue (friends, "data") as IList;
				
				if (data != null)
				{
					foreach (Dictionary<string, object> item in data)
					{
						IFacebookFriendProfile friend = new FacebookFriendProfile ();
						PopulateProfile (item, friend);
						
						result.data.Add (friend);
					}
				}
			}

			return result;
		}
		
		public static void PopulateProfile (Dictionary<string, object> data, IFacebookProfile profile)
		{
			profile.Id = GetDictionaryValue (data, "id") as string;
			profile.FirstName = GetDictionaryValue (data, "first_name") as string;
			profile.LastName = GetDictionaryValue (data, "last_name") as string;
		}
		
		public static object GetDictionaryValue (Dictionary<string, object> dictionary, string key)
		{
			object result;
			
			dictionary.TryGetValue(key, out result);
			
			return result;
		}
	}
}