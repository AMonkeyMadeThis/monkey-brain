﻿namespace Amonkeymadethis.Common.Facebook.Controller
{
	using UnityEngine;
	using Amonkeymadethis.Common.Monkeybrain.Attribute.Inject;
	using Amonkeymadethis.Common.Monkeybrain.Command.Result;
	using Amonkeymadethis.Common.Monkeybrain.Helper;
	using Amonkeymadethis.Common.Monkeybrain.Responder;
	using Amonkeymadethis.Common.Monkeybrain.Util;
	using Amonkeymadethis.Common.Facebook.Command.Initialisation;
	using Amonkeymadethis.Common.Facebook.Command.Authentication;
	using Amonkeymadethis.Common.Facebook.Command.Profile;
	using Amonkeymadethis.Common.Facebook.Context;
	using Amonkeymadethis.Common.Facebook.Model;
	using Amonkeymadethis.Common.Facebook.UI.View;

	[RequireComponent(typeof(FacebookViewStack))]
	/**
	 * FacebookController
	 */
	public class FacebookController : MonkeyHead
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Managed
		// ----------------------------------

		[Inject]
		/**
		 * Model
		 */
		public FacebookModel Model { get; set; }
		
		// ----------------------------------
		// Components
		// ----------------------------------

		private FacebookViewStack viewStack;

		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Lifecycle
		// ----------------------------------
		
		/**
		 * Start
		 */
		public override void Start ()
		{
			base.Start ();

			CollectComponents ();
			InitFacebook ();

			SetSelectedContent (FacebookContent.Hidden);
		}
		
		// ----------------------------------
		// UI
		// ----------------------------------

		protected void CollectComponents ()
		{
			viewStack = GetComponent<FacebookViewStack> ();
		}

		protected void SetSelectedContent (FacebookContent selected)
		{
			if (viewStack)
				viewStack.SelectedContent = selected;
		}

		// ----------------------------------
		// InitFacebook
		// ----------------------------------

		protected void InitFacebook ()
		{
			LogUtil.LogCustomEvent (this, "InitFacebook");

			InitFacebookRequest message = new InitFacebookRequest ();
			message.OnInitComplete = OnInitComplete;
			message.OnHideUnity = OnHideUnity;

			DispatchMessage (message);

			SetSelectedContent (FacebookContent.Initialising);
		}

		private void OnInitComplete()
		{
			LogUtil.LogCustomEvent (this, "OnInitComplete");
			
			SetSelectedContent (FacebookContent.UnAuthenticated);
		}
		
		private void OnHideUnity(bool isGameShown)
		{
			Debug.Log ("Is game showing? " + isGameShown);
			// Start timer or something to log out?
		}

		// ----------------------------------
		// Log in
		// ----------------------------------
		
		protected void LogIn ()
		{
			FacebookLogInRequest message = new FacebookLogInRequest ();
			message.Permissions = Scope.Basic;
			message.Responder = new Responder (LogInResult, LogInFault);

			DispatchMessage (message);
			
			SetSelectedContent (FacebookContent.Authenticating);
		}
		
		protected void LogInResult (ICommandResponse result)
		{
			LogUtil.LogCustomEvent (this, "LogInResult", result);
			
			SetSelectedContent (FacebookContent.Authenticated);

			LoadProfile ();
		}
		
		protected void LogInFault (ICommandResponse result)
		{
			LogUtil.LogCustomEvent (this, "LogInFault", result);
		}
		
		// ----------------------------------
		// Load Profile
		// ----------------------------------
		
		protected void LoadProfile ()
		{
			LoadFacebookProfileRequest message = new LoadFacebookProfileRequest ();
			message.Profile = FacebookProfileQuery.FullProfile;
			message.Responder = new Responder (LoadProfileResult, LoadProfileFault);
			
			DispatchMessage (message);
		}
		
		protected void LoadProfileResult (ICommandResponse result)
		{
			LogUtil.LogCustomEvent (this, "LoadProfileResult", result);
		}
		
		protected void LoadProfileFault (ICommandResponse result)
		{
			LogUtil.LogCustomEvent (this, "LoadProfileFault", result);
		}
	}
}