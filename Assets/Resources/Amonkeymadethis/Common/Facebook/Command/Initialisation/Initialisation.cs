﻿/**
 * 
 */
namespace Amonkeymadethis.Common.Facebook.Command.Initialisation
{
	using Amonkeymadethis.Common.Monkeybrain.Command;
	using Amonkeymadethis.Common.Monkeybrain.Command.Request;
	using Amonkeymadethis.Common.Monkeybrain.Util;

	#region Initialisation

	/**
	 * InitFacebookRequest
	 */
	public class InitFacebookRequest : AbstractCommandRequest
	{
		// ---------------------------------------------------------------------
		//
		// Delegates
		//
		// ---------------------------------------------------------------------

		public delegate void InitDelegate ();
		public delegate void HideDelegate (bool isGameShown);

		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Facebook Callback / Events
		// ----------------------------------

		/**
		 * OnInitComplete
		 */
		public InitDelegate OnInitComplete { get; set; }
		/**
		 * OnHideUnity
		 */
		public HideDelegate OnHideUnity { get; set; }
	}

	/**
	 * InitFacebookCommand
	 */
	public class InitFacebookCommand : AbstractCommand
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Request
		 */
		public InitFacebookRequest Request { get { return Payload as InitFacebookRequest; } }
		
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		/**
		 * Execute
		 */
		public override void Execute ()
		{
			LogUtil.LogCustomEvent (this, "Execute");

			FB.Init
			(
				delegate() { Request.OnInitComplete.Invoke (); },
				delegate(bool isGameShown) { Request.OnHideUnity.Invoke (isGameShown); }
			);

			// Done what we need to now go home!
			Result ();
		}
	}

	#endregion
}