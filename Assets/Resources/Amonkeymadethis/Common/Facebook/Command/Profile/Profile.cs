/**
 * 
 */
namespace Amonkeymadethis.Common.Facebook.Command.Profile
{
	using System;
	using Facebook;
	using UnityEngine;
	using System.Collections.Generic;
	using Amonkeymadethis.Common.Monkeybrain.Attribute.Inject;
	using Amonkeymadethis.Common.Monkeybrain.Command;
	using Amonkeymadethis.Common.Monkeybrain.Command.Request;
	using Amonkeymadethis.Common.Monkeybrain.Util;
	using Amonkeymadethis.Common.Facebook.Model;
	using Amonkeymadethis.Common.Facebook.Util;

	#region Query

	[Flags]
	/**
	 * BitFlag enum of query fields
	 */
	public enum FacebookAPIQuery
	{
		Version = 1 << 0,
		ProfileFields = 1 <<1,
		Me = 1 << 2,
		Friends = 1 << 3,
		InvitableFriends = 1 << 4,
	}

	public class FacebookProfileQuery
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * Basic Profile
		 */
		public static FacebookAPIQuery BasicProfile { get { return FacebookAPIQuery.Version | FacebookAPIQuery.Me; } }
		/**
		 * Basic Profile, Friends & Invitables
		 */
		public static FacebookAPIQuery FullProfile { get { return FacebookAPIQuery.Version | FacebookAPIQuery.Me | FacebookAPIQuery.Friends | FacebookAPIQuery.InvitableFriends; } }

		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		public static string GetURI (FacebookAPIQuery flags)
		{
			List<string> sections = new List<string> ();
			
			foreach(FacebookAPIQuery flag in Enum.GetValues(typeof(FacebookAPIQuery)))
			{
				bool hasFlag = (flags & flag) == flag;
				
				if (hasFlag)
				{
					string value = QueryString.GetSubstitutedSection (flag);
					
					sections.Add (value);
				}
			}
			
			return string.Join (string.Empty, sections.ToArray ());
		}
	}

	public class QueryString
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Sections of query string
		 */
		public static Dictionary<string, string> Values = new Dictionary<string, string>
		{
			{"Version", "/v2.2"},
			{"ProfileFields", "id,first_name,last_name,picture.width(128).height(128)"},
			{"Me","/me?fields={0},"},
			{"Friends","friends.limit({1}).fields({0}),"},
			{"InvitableFriends","invitable_friends.limit({1}).fields({0})"}
		};
		
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		public static string GetSection (FacebookAPIQuery flag)
		{
			string key = flag.ToString ();
			string value;
			
			QueryString.Values.TryGetValue (key, out value);

			return value;
		}

		public static string GetSubstitutedSection (FacebookAPIQuery flag, int limitresults = 1000)
		{
			string profileFields = GetSection (FacebookAPIQuery.ProfileFields);
			string pattern = GetSection (flag);
			string result = string.Format (pattern, profileFields, limitresults);

			return result;
		}
	}

	#endregion

	#region Initialisation
	
	/**
	 * LoadFacebookProfileRequest
	 */
	public class LoadFacebookProfileRequest : AbstractCommandRequest
	{
		// ---------------------------------------------------------------------
		//
		// Delegates
		//
		// ---------------------------------------------------------------------

		/**
		 * InitDelegate
		 */
		public delegate void InitDelegate ();
		/**
		 * HideDelegate
		 */
		public delegate void HideDelegate (bool isGameShown);
		
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * Profile
		 */
		public FacebookAPIQuery Profile;

		// ----------------------------------
		// Facebook Callback / Events
		// ----------------------------------
		
		/**
		 * OnInitComplete
		 */
		public InitDelegate OnInitComplete { get; set; }
		/**
		 * OnHideUnity
		 */
		public HideDelegate OnHideUnity { get; set; }
	}
	
	/**
	 * LoadFacebookProfileCommand
	 */
	public class LoadFacebookProfileCommand : AbstractCommand
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Managed
		// ----------------------------------

		[Inject]
		/**
		 * Model
		 */
		public FacebookModel Model { get; set; }

		// ----------------------------------
		// Helper Flags
		// ----------------------------------

		/**
		 * Request
		 */
		public LoadFacebookProfileRequest Request { get { return Payload as LoadFacebookProfileRequest; } }
		/**
		 * Tries
		 */
		protected int tries = 3;
		
		// ----------------------------------
		// Helper Flags
		// ----------------------------------

		/**
		 * KeepTrying
		 */
		public bool GetKeepTrying() { return --tries >= 0; }

		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Execute
		 */
		public override void Execute ()
		{
			LogUtil.LogCustomEvent (this, "Execute");

			LoadProfile ();
		}

		protected void LoadProfile ()
		{
			if (GetKeepTrying () && Request != null && Request.Profile != null)
			{
				string uri = FacebookProfileQuery.GetURI (Request.Profile);
				FB.APIBridge(uri, LoadProfileCallback);
			}
			else
			{
				Failure ();
			}
		}

		void LoadProfileCallback(FBResult result)
		{
			bool hasError = (result.Error != null);

			LogUtil.LogCustomEvent (this, "LoadProfileCallback. Error=" + hasError);

			if (hasError)
			{
				LoadProfile ();
			}
			else
			{
				// Deserialize result
				FacebookUtil.DeserialiseData (result.Text, Model);
				// Load Pictures
				LoadPictures ();
			}
		}

		protected void LoadPictures ()
		{
			// CoRoutine ... 

			// Reqest player info and profile picture
			//LoadPictureAPI(Util.GetPictureURL("me", 128, 128),MyPictureCallback);

			// Done what we need to now go home!
			Result (Model.Player);
		}
	}
	
	#endregion
}