/**
 * 
 */
namespace Amonkeymadethis.Common.Facebook.Command.Authentication
{
	using System;
	using UnityEngine;
	using Amonkeymadethis.Common.Monkeybrain.Command;
	using Amonkeymadethis.Common.Monkeybrain.Command.Request;
	using Amonkeymadethis.Common.Monkeybrain.Util;

	#region Permissions

	[Flags]
	/**
	 * BitFlag enum of permissions
	 */
	public enum FacebookPermission
	{
		public_profile = 1 << 0,
		email = 1 << 1,
		user_friends = 1 << 2,
		publish_actions = 1 << 3,
	}

	public class Scope
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * Basic non spammy permissions
		 */
		public static FacebookPermission Basic { get { return FacebookPermission.public_profile | FacebookPermission.email | FacebookPermission.user_friends; } }
		/**
		 * Full spammy permissions
		 */
		public static FacebookPermission Full { get { return FacebookPermission.publish_actions; } }
		
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		public static string GetBasicPermissionsString()
		{
			return Basic.ToString ();
		}
		
		public static string GetFullPermissionsString()
		{
			return Basic.ToString ();
		}
	}

	#endregion

	#region LogInRequest

	/**
	 * FacebookLogInRequest
	 */
	public class FacebookLogInRequest : AbstractCommandRequest
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * Requested permissions
		 */
		public FacebookPermission Permissions { get; set; }
	}

	#endregion

	#region LogIn

	/**
	 * FacebookLogInCommand
	 */
	public class FacebookLogInCommand : AbstractCommand
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		// ----------------------------------
		// Command
		// ----------------------------------

		/**
		 * Request
		 */
		public FacebookLogInRequest Request { get { return Payload as FacebookLogInRequest; } }

		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Execute
		 */
		public override void Execute ()
		{
			LogUtil.LogCustomEvent (this, "Execute");

			string scope = Request.Permissions.ToString ();

			FB.Login (scope, OnLogInComplete);
		}
		
		protected void OnLogInComplete (FBResult result)
		{
			LogUtil.LogCustomEvent (this, "OnLogInComplete");

			bool hasError = (result.Error != null);

			if (!hasError)
			{
				LogUtil.LogCustomEvent (this, "OnLogInCompleteResult");
				Result (result.Text);
			}
			else
			{
				LogUtil.LogCustomEvent (this, "OnLogInCompleteFault");
				Failure ();
			}
		}
	}

	#endregion
	
	#region LogOutRequest
	
	/**
	 * FacebookLogOutRequest
	 */
	public class FacebookLogOutRequest : AbstractCommandRequest
	{
		public delegate void InitDelegate ();
		public delegate void HideDelegate (bool isGameShown);
		
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Facebook Callback / Events
		// ----------------------------------
		
		/**
		 * OnInitComplete
		 */
		public InitDelegate OnInitComplete { get; set; }
		/**
		 * OnHideUnity
		 */
		public HideDelegate OnHideUnity { get; set; }
	}

	#endregion

	#region LogOut

	/**
	 * FacebookLogOutCommand
	 */
	public class FacebookLogOutCommand : AbstractCommand
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Request
		 */
		public FacebookLogOutRequest Request { get { return Payload as FacebookLogOutRequest; } }
		
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Execute
		 */
		public override void Execute ()
		{
			LogUtil.LogCustomEvent (this, "Execute");

			// Log out
			FB.Logout ();
			// Done what we need to now go home!
			Result ();
		}
	}
	
	#endregion
}