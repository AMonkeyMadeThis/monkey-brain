﻿namespace Amonkeymadethis.Common.Facebook.Context
{
	using Amonkeymadethis.Common.Monkeybrain.Context;
	using Amonkeymadethis.Common.Facebook.Command.Initialisation;
	using Amonkeymadethis.Common.Facebook.Command.Authentication;
	using Amonkeymadethis.Common.Facebook.Command.Profile;
	using Amonkeymadethis.Common.Facebook.Model;

	/**
	 * FacebookContext
	 */
	public class FacebookContext : AbstractContext
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		// ----------------------------------
		// Mapping
		// ----------------------------------
		
		/**
		 * Map Types
		 */
		public override void MapTypes ()
		{
			// Singletons
			MapSingleton (typeof(FacebookModel), typeof(FacebookModel));

			// Commands
			MapCommand (typeof(InitFacebookRequest), typeof(InitFacebookCommand));
			MapCommand (typeof(FacebookLogInRequest), typeof(FacebookLogInCommand));
			MapCommand (typeof(FacebookLogOutRequest), typeof(FacebookLogOutCommand));
			MapCommand (typeof(LoadFacebookProfileRequest), typeof(LoadFacebookProfileCommand));
		}
	}
}
