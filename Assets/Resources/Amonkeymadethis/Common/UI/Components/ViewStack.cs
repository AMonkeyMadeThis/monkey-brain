﻿namespace Amonkeymadethis.Common.UI.Components
{
	using UnityEngine;
	using System.Collections;

	/**
	 * ViewStack
	 */
	public class ViewStack : DisplayObject
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// SelectedIndex
		// ----------------------------------

		private int selectedIndex;

		/**
		 * SelectedIndex
		 */
		public virtual int SelectedIndex
		{
			get { return selectedIndex; }
			set
			{
				if (selectedIndex != value)
				{
					selectedIndex = value;
					Invalidate ();
				}
			}
		}

		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		// ----------------------------------
		// Invalidation
		// ----------------------------------

		protected override void UpdateChildren ()
		{
			foreach (var child in Children)
			{
				var navigatorContent = child as INavigatorContent;

				if (navigatorContent != null)
				{
					bool isSelected = navigatorContent.ContentIndex == SelectedIndex;

					navigatorContent.Visible = isSelected;
				}
			}
		}
	}
}