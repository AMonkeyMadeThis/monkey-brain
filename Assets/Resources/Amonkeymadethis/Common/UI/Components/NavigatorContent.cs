﻿namespace Amonkeymadethis.Common.UI.Components
{
	using UnityEngine;
	using System.Collections;
	
	[RequireComponent(typeof(Canvas))]
	/**
	 * NavigatorContent, a view to be managed by a ViewStack
	 */
	public class NavigatorContent : DisplayObject, INavigatorContent
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Visible
		// ----------------------------------

		/**
		 * Toggle visibility
		 */
		public override bool Visible
		{
			get { return Canvas.enabled; }
			set
			{
				Canvas.enabled = value;
			}
		}

		// ----------------------------------
		// SelectedIndex
		// ----------------------------------

		/**
		 * ContentIndex
		 */
		public virtual int ContentIndex { get; set; }
		
		// ----------------------------------
		// Helpers
		// ----------------------------------

		/**
		 * Canvas
		 */
		public Canvas Canvas { get { return GetComponent<Canvas> (); } }
	}
}