namespace Amonkeymadethis.Common.UI.Components
{
	using UnityEngine;

	/**
	 * INavigatorContent, a view to be managed by a ViewStack
	 */
	public interface INavigatorContent : IDisplayObject
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// ContentIndex
		// ----------------------------------

		/**
		 * ContentIndex
		 */
		int ContentIndex { get; set; }
		
		// ----------------------------------
		// Helpers
		// ----------------------------------

		/**
		 * Canvas
		 */
		Canvas Canvas { get; }
	}
}