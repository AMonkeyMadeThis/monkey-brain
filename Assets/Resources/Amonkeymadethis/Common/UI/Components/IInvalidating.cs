namespace Amonkeymadethis.Common.UI.Components
{
	/**
	 * IInvalidating
	 */
	public interface IInvalidating
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------

		// ----------------------------------
		// Invalidation
		// ----------------------------------

		/**
		 * Invalidates the object causing it to re validate itself
		 */
		void Invalidate ();
	}
}