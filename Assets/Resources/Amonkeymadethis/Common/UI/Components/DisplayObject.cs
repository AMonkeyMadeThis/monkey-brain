namespace Amonkeymadethis.Common.UI.Components
{
	using UnityEngine;

	/**
	 * DisplayObject
	 */
	public class DisplayObject : MonoBehaviour, IDisplayObject
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Toggle visibility
		 */
		public virtual bool Visible { get; set; }
		/**
		 * Children as collection of IDisplayObjects
		 */
		public IDisplayObject[] Children { get; set; }
		
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Lifecycle
		// ----------------------------------

		public virtual void Start ()
		{
			CollectChildren ();
			Invalidate ();
		}
		
		public virtual void CollectChildren ()
		{
			Children = GetComponentsInChildren<IDisplayObject> ();
		}
		
		public virtual void LateUpdate ()
		{
			// Override to add functionality
		}

		// ----------------------------------
		// Invalidation
		// ----------------------------------

		public virtual void Invalidate ()
		{
			CommitProperties ();
			InvalidateParent ();
		}
		
		protected virtual void CommitProperties ()
		{
			UpdateChildren ();
		}

		protected virtual void UpdateChildren ()
		{
			
		}

		protected virtual void InvalidateParent ()
		{
			var parent = transform.parent as IDisplayObject;

			if (parent != null)
			{
				parent.Invalidate ();
			}
		}
	}
}