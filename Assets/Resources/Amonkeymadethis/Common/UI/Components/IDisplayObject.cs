namespace Amonkeymadethis.Common.UI.Components
{
	public interface IDisplayObject : IInvalidating
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------

		/**
		 * Toggle visibility
		 */
		bool Visible { get; set; }
		/**
		 * Children as collection of IDisplayObjects
		 */
		IDisplayObject[] Children { get; set; }
	}
}